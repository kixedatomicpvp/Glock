package glockteam.glock.events;

import com.darkmagician6.eventapi.events.Event;

/**
 * Created by Kix on 12/23/2016.
 */
public class EventKeyPress implements Event {

    private final int eventKey;

    public EventKeyPress(final int eventKey) {
        this.eventKey = eventKey;
    }

    public int getEventKey() {
        return this.eventKey;
    }

}
