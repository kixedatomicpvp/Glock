package glockteam.glock.events;

import com.darkmagician6.eventapi.events.callables.EventCancellable;
import com.darkmagician6.eventapi.types.EventType;
import net.minecraft.client.entity.EntityPlayerSP;

/**
 * Created by Kix on 12/22/2016.
 */
public class UpdateEvent extends EventCancellable {

    private EntityPlayerSP player;

    public boolean alwaysSend;
    public double y;
    public float yaw, pitch;
    public boolean onGround;
    public byte type;

    public UpdateEvent(double y, float[] rot, boolean onGround, EntityPlayerSP player) {
        this.y = y;
        this.player = player;
        this.yaw = rot[0];
        this.pitch = rot[1];
        this.onGround = onGround;
        this.type = EventType.PRE;
    }

    public UpdateEvent() {
        this.type = EventType.POST;
    }

    public EntityPlayerSP getPlayer() {
        return player;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }
}
