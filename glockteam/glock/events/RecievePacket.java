package glockteam.glock.events;

import com.darkmagician6.eventapi.events.callables.EventCancellable;
import net.minecraft.network.Packet;

/**
 * Created by Kix on 12/23/2016.
 */
public class RecievePacket extends EventCancellable {

    private Packet packet;

    public Packet getPacket() {
        return this.packet;
    }

    public void setPacket(final Packet packet) {
        this.packet = packet;
    }

    public RecievePacket(final Packet packet) {
        this.packet = packet;
    }

}
