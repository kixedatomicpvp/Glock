package glockteam.glock.events;

import com.darkmagician6.eventapi.events.Event;

/**
 * Created by Kix on 12/22/2016.
 */
public class MotionEvent implements Event {

    public double x;
    public double y;
    public double z;
    public float yaw, pitch;

    public MotionEvent(final double x, final double y, final double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public MotionEvent(final double x, final double y, final double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }


}
