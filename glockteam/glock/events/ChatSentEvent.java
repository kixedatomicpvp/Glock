package glockteam.glock.events;

import com.darkmagician6.eventapi.events.callables.EventCancellable;
import glockteam.glock.Glock;
import glockteam.glock.command.Command;
import glockteam.glock.utils.util.ChatUtil;

/**
 * Created by Kix on 12/23/2016.
 */
public class ChatSentEvent extends EventCancellable {
    private String message;

    public ChatSentEvent(String message) {
        this.message = message;
    }


    public void checkForCommands() {
        if (message.startsWith(".")) {
            for (final Command command : Glock.commandManager
                    .getContents()) {
                if (message.split(" ")[0].equalsIgnoreCase("."
                        + command.getName())) {
                    try {
                        command.send(message);
                    } catch (final Exception e) {
                        ChatUtil.printLn("Invalid arguments! "
                                + command.getName() + " "
                                + command.getSyntax());
                    }
                    this.setCancelled(true);
                } else {
                    for (final String alias : command.getAliases()) {
                        if (message.split(" ")[0].equalsIgnoreCase("." + alias)) {
                            try {
                                command.send(message);
                            } catch (final Exception e) {
                                ChatUtil.printLn("Invalid arguments! " + alias
                                        + " " + command.getSyntax());
                            }
                            this.setCancelled(true);
                        }
                    }
                }
            }

            if (!this.isCancelled()) {
                ChatUtil.printLn("Command \"" + message + "\" was not found!");
                this.setCancelled(true);
            }
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
