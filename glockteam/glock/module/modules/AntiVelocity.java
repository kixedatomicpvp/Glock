package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import glockteam.glock.events.RecievePacket;
import glockteam.glock.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 12/23/2016.
 */
public class AntiVelocity extends Module {

    public AntiVelocity() {
        super("AntiVelocity", Type.COMBAT, 0xFFCAC6FF);
        this.setBind(Keyboard.KEY_P);
    }

    @EventTarget
    public void onPacket(RecievePacket event) {
        if (event.getPacket() instanceof S12PacketEntityVelocity) {
            S12PacketEntityVelocity packet = (S12PacketEntityVelocity) event.getPacket();
            if (Minecraft.getMinecraft().theWorld.getEntityByID(packet.func_149412_c()) == Minecraft.getMinecraft().thePlayer) {
                event.setCancelled(true);
            }
        }
    }

}
