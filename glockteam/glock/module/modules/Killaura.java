package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import com.darkmagician6.eventapi.types.EventType;
import com.google.common.io.Files;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import glockteam.glock.Glock;
import glockteam.glock.command.Command;
import glockteam.glock.events.MotionEvent;
import glockteam.glock.events.UpdateEvent;
import glockteam.glock.module.Module;
import glockteam.glock.utils.file.AbstractFile;
import glockteam.glock.utils.file.Value;
import glockteam.glock.utils.util.ChatUtil;
import glockteam.glock.utils.util.TimerUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import org.lwjgl.input.Keyboard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kix on 12/22/2016.
 */
public class Killaura extends Module {

    public static Value<Float> fov;
    public static Value<Double> range;
    public static Value<Integer> aps;
    public static Value<Integer> existed;
    public static Value<Boolean> autoBlock;
    public static Value<Boolean> invisibles;
    public static Value<Boolean> players;
    public static Value<Boolean> animals;
    public static Value<Boolean> mobs;
    public static Value<Boolean> lockview;
    public static Value<Boolean> swordOnly;
    public static TimerUtil timer;
    public int delay;
    public float oldPitch, oldYaw;

    public Killaura() {
        super("Killaura", Type.COMBAT, 0xFFFF0000);
        this.setBind(Keyboard.KEY_R);
        timer = new TimerUtil();
        fov = new Value<Float>("fov", 180F, 10F, 360F);
        range = new Value<Double>("range", 4.4, 3.5, 6.0);
        aps = new Value<Integer>("aps", 10, 6, 15);
        existed = new Value<Integer>("ticks", 0, 0, 500);
        autoBlock = new Value<Boolean>("autoblock", false);
        invisibles = new Value<Boolean>("invisibles", false);
        players = new Value<Boolean>("players", true);
        animals = new Value<Boolean>("animals", false);
        mobs = new Value<Boolean>("mobs", true);
        lockview = new Value<Boolean>("lockview", true);
        swordOnly = new Value<Boolean>("swordonly", false);
        getValues().add(fov);
        getValues().add(range);
        getValues().add(aps);
        getValues().add(existed);
        getValues().add(autoBlock);
        getValues().add(invisibles);
        getValues().add(players);
        getValues().add(animals);
        getValues().add(mobs);
        getValues().add(lockview);
        getValues().add(swordOnly);
    }

    @Override
    public void onEnable() {
        timer.reset();
        super.onEnable();
    }

    @Override
    public void onDisable() {
        timer.reset();
        mc.gameSettings.keyBindUseItem.pressed = false;
        super.onDisable();
    }

    @EventTarget
    public void onUpdate(UpdateEvent event){
        if (this.isEnabled()) {
            this.oldPitch = event.pitch;
            this.oldYaw = event.yaw;
            EntityLivingBase entity = (EntityLivingBase) ka.getBestEntity(range.getValue(), 180F, false, 0,
                    invisibles.getValue(), players.getValue(), mobs.getValue(),animals.getValue());
            if (entity != null) {
                if(!lockview.getValue()) {
                    event.yaw = ka.getRotationsNeeded(entity)[0];
                    event.pitch = ka.getRotationsNeeded(entity)[1];
                }
                for (Object entity1 : mc.theWorld.loadedEntityList) {
                    if (((Entity) entity1).isInvisible() && entity1 != mc.thePlayer) {
                        mc.theWorld.removeEntity((Entity) entity1);
                    }
                }

                if (autoBlock.getValue()) {
                    if (mc.thePlayer.getHeldItem().getItem() instanceof ItemSword) {
                        final ItemStack currentItem = mc.thePlayer.getCurrentEquippedItem();
                        this.mc.thePlayer.setItemInUse(currentItem, currentItem.getMaxItemUseDuration());
                    }
                }

                if (event.type == EventType.POST) {
                    if (timer.hasReached(1000 / aps.getValue())) {
                        timer.reset();
                        ka.attackTarget(entity);
                    }
                }
            }
        }
    }

    @EventTarget
    public void onMotion(MotionEvent event){

        EntityLivingBase entity = (EntityLivingBase) ka.getBestEntity(range.getValue(), 180F, false, 0,
                invisibles.getValue(), players.getValue(), mobs.getValue(),animals.getValue());

        if(lockview.getValue()) {

            if (entity != null) {
                mc.thePlayer.rotationYaw = ka.getRotationsNeeded(entity)[0];
                mc.thePlayer.rotationPitch = ka.getRotationsNeeded(entity)[1];
            }
        }
    }

}
