package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import glockteam.glock.events.MotionEvent;
import glockteam.glock.module.Module;
import glockteam.glock.utils.util.TimerUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.init.Items;
import net.minecraft.item.ItemSoup;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 12/23/2016.
 */
public class AutoSoup extends Module {

    private final TimerUtil time = new TimerUtil();

    public AutoSoup() {
        super("AutoSoup", Type.COMBAT, 0x42f4c5);
        this.setBind(Keyboard.KEY_Z);
    }

    private boolean doesHotbarHaveSoups() {
        for (int index = 36; index < 45; index++) {
            final ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(
                    index).getStack();
            if (stack == null) {
                continue;
            }
            if (isStackSoup(stack))
                return true;
        }
        return false;
    }

    private void eatSoup() {
        for (int index = 36; index < 45; index++) {
            final ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(
                    index).getStack();
            if (stack == null) {
                continue;
            }
            if (isStackSoup(stack)) {
                stackBowls();
                final int oldslot = mc.thePlayer.inventory.currentItem;
                mc.getNetHandler().addToSendQueue(
                        new C09PacketHeldItemChange(index - 36));
                mc.playerController.updateController();
                mc.getNetHandler().addToSendQueue(
                        new C08PacketPlayerBlockPlacement(
                                stack));
                mc.getNetHandler().addToSendQueue(
                        new C09PacketHeldItemChange(oldslot));
                break;
            }
        }
    }

    private void getSoupFromInventory() {
        if (mc.currentScreen instanceof GuiChest)
            return;
        stackBowls();
        for (int index = 9; index < 36; index++) {
            final ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(
                    index).getStack();
            if (stack == null) {
                continue;
            }

            if (isStackSoup(stack)) {
                mc.playerController.windowClick(0, index, 0, 1, mc.thePlayer);
                break;
            }
        }
    }

    private boolean isStackSoup(ItemStack stack) {
        if (stack == null)
            return false;
        return stack.getItem() instanceof ItemSoup;
    }

    private void stackBowls() {
        if (mc.currentScreen instanceof GuiChest)
            return;
        for (int index = 9; index < 45; index++) {
            final ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(
                    index).getStack();
            if (stack == null) {
                continue;
            }

            if (stack.getItem() == Items.bowl) {
                mc.playerController.windowClick(0, index, 0, 0, mc.thePlayer);
                mc.playerController.windowClick(0, 9, 0, 0, mc.thePlayer);
                break;
            }
        }
    }

    @EventTarget
    public void onMotion(MotionEvent event){
        if (updateCounter() == 0)
            return;
        if (mc.thePlayer.getHealth() <= 10.0F
                && time.hasReached(500L)) {
            if (doesHotbarHaveSoups()) {
                eatSoup();
            } else {
                getSoupFromInventory();
            }
            time.reset();
        }
    }

    private int updateCounter() {
        int counter = 0;
        for (int index = 9; index < 45; index++) {
            final ItemStack stack = mc.thePlayer.inventoryContainer.getSlot(
                    index).getStack();
            if (stack == null) {
                continue;
            }
            if (isStackSoup(stack)) {
                counter += stack.stackSize;
            }
        }
        return counter;
    }

}
