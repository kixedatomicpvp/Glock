package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import glockteam.glock.Glock;
import glockteam.glock.events.RenderGameOverlayEvent;
import glockteam.glock.gui.SimpleTabGui;
import glockteam.glock.module.Module;
import glockteam.glock.utils.file.Value;
import net.minecraft.client.gui.ScaledResolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Kix on 12/22/2016.
 */
public class Overlay extends Module {

    private Value<Boolean> stamina = new Value<Boolean>("Stamina", true);

    public Overlay() {
        super("Overlay", Type.RENDER, 0xFF00A6);
        this.setEnabled(true);
        this.setHidden(true);
    }

    @EventTarget
    public void render(RenderGameOverlayEvent event) {
        ScaledResolution var2 = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
        int yCount = 2;
        if (!mc.gameSettings.showDebugInfo) {
            fr.drawStringWithShadow("\247f" + tag + " \2477v" + version, 2, 2, 0xFFD4C390);
            SimpleTabGui.drawTabGui();

            List<Module> mods = new ArrayList<Module>();
            for (Module module : Glock.moduleManager.getContents()) mods.add(module);
            Collections.sort(mods, new ModuleComparator());
            for (Module module : mods) {
                if (module.isEnabled() && !module.isHidden()) {
                    int width = fr.getStringWidth(module.getName()) + 3;
                    fr.drawStringWithShadow(module.getName(), var2.getScaledWidth() - fr.getStringWidth(module.getName()) - 3, yCount, module.getColor());
                    yCount += 10;
                }
            }
        }
    }

    public static class ModuleComparator implements Comparator<Module> {

        @Override
        public int compare(Module o1, Module o2) {
            if (fr.getStringWidth(o1.getName()) < fr.getStringWidth(o2.getName())) {
                return 1;
            } else if (fr.getStringWidth(o1.getName()) > fr.getStringWidth(o2.getName())) {
                return -1;
            } else {
                return 0;
            }
        }

    }

}

