package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import glockteam.glock.events.UpdateEvent;
import glockteam.glock.module.Module;
import net.minecraft.network.play.client.C0APacketAnimation;

import java.awt.*;

/**
 * Created by Kix on 12/24/2016.
 */
public class Retard extends Module {

    public Retard() {
        super("Retard", Type.MISCELLANEOUS, new Color(125, 219, 255, 255).getRGB());
    }

    private float spin;
    @EventTarget
    public void onUpdate(UpdateEvent eventUpdate) {
        spin += 20;
        if (spin > 180) {
            spin = -180;
        } else if (spin < -180) {
            spin = 180;
        }
        eventUpdate.setYaw(spin);
        eventUpdate.setPitch(-180);
        mc.getNetHandler().addToSendQueue(
                new C0APacketAnimation());
    }

}
