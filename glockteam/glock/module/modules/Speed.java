package glockteam.glock.module.modules;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import glockteam.glock.events.UpdateEvent;
import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventTarget;
import com.darkmagician6.eventapi.types.EventType;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import glockteam.glock.module.Module;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.block.BlockLiquid;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.potion.Potion;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

/**
 * Created by Kix on 12/23/2016.
 */
public class Speed extends Module {

    private int stage;
    private double moveSpeed;
    private double lastDist;
    private int timeTick = 0;

    public Speed() {
        super("Speed", Type.MOVEMENT, 0xFFFFA07F);
        this.setBind(Keyboard.KEY_N);
    }

    @Override
    public void onDisable() {
        mc.timer.timerSpeed = 1f;
        super.onDisable();
    }

    public float getDirection() {
        float yaw = this.mc.thePlayer.rotationYaw;
        if (this.mc.thePlayer.moveForward < 0) {
            yaw += 180;
        }
        float forward = 1;
        if (this.mc.thePlayer.moveForward < 0) {
            forward = -0.5F;
        } else if (this.mc.thePlayer.moveForward > 0) {
            forward = 0.5F;
        }
        if (this.mc.thePlayer.moveStrafing > 0) {
            yaw -= 90 * forward;
        }
        if (this.mc.thePlayer.moveStrafing < 0) {
            yaw += 90 * forward;
        }
        yaw *= 0.017453292F;
        return yaw;
    }

    private double getBaseMoveSpeed() {
        double baseSpeed = 0.2873;
        if (this.mc.thePlayer.isPotionActive(Potion.moveSpeed)) {
            int amplifier = this.mc.thePlayer.getActivePotionEffect(Potion.moveSpeed).getAmplifier();
            baseSpeed *= 1 + 0.2 * (amplifier + 1);
        }
        return baseSpeed;
    }

    public float getSpeed() {
        return (float) Math.sqrt((this.mc.thePlayer.motionX * this.mc.thePlayer.motionX)
                + (this.mc.thePlayer.motionZ * this.mc.thePlayer.motionZ));
    }

    @EventTarget
    public void onUpdate(UpdateEvent event) {
        if (event.type == EventType.PRE) {
            if (!(this.isOnLiquid() || this.isInLiquid())) {
                if (isMoving(this.mc.thePlayer) && this.mc.thePlayer.onGround) {
                    if (this.mc.thePlayer.ticksExisted % 2 != 0 && !this.mc.isSingleplayer()
                            && !this.mc.thePlayer.movementInput.jump) {
                        event.y = this.mc.thePlayer.posY + 0.4;
                        event.onGround = false;
                    }
                    float speed = this.mc.thePlayer.ticksExisted % 2 == 0 ? 0.15f * 2.32F : 0.15F;
                    this.mc.thePlayer.motionX = -(Math.sin(this.getDirection()) * speed);
                    this.mc.thePlayer.motionZ = Math.cos(this.getDirection()) * speed;

                    mc.timer.timerSpeed = 1.095f;
                }
            }
        }
    }

    public boolean isInLiquid(){
        if(mc.thePlayer == null)
            return false;
        boolean inLiquid = false;
        int y = (int) mc.thePlayer.boundingBox.minY;
        for(int x = MathHelper.floor_double(mc.thePlayer.boundingBox.minX); x < MathHelper
                .floor_double(mc.thePlayer.boundingBox.maxX) + 1; x++){
            for(int z = MathHelper.floor_double(mc.thePlayer.boundingBox.minZ); z < MathHelper
                    .floor_double(mc.thePlayer.boundingBox.maxZ) + 1; z++){
                Block block = mc.theWorld.getBlockState(new BlockPos(x, y, z))
                        .getBlock();
                if((block != null) && (!(block instanceof BlockAir))){
                    if(!(block instanceof BlockLiquid))
                        return false;
                    inLiquid = true;
                }
            }
        }
        return inLiquid;
    }

    public boolean isOnLiquid(){
        if(mc.thePlayer == null)
            return false;
        boolean onLiquid = false;
        int y = (int) mc.thePlayer.boundingBox.offset(0.0D, -0.01D, 0.0D).minY;
        for(int x = MathHelper.floor_double(mc.thePlayer.boundingBox.minX); x < MathHelper
                .floor_double(mc.thePlayer.boundingBox.maxX) + 1; x++){
            for(int z = MathHelper.floor_double(mc.thePlayer.boundingBox.minZ); z < MathHelper
                    .floor_double(mc.thePlayer.boundingBox.maxZ) + 1; z++){
                Block block = mc.theWorld.getBlockState(new BlockPos(x, y, z))
                        .getBlock();
                if((block != null) && (!(block instanceof BlockAir))){
                    if(!(block instanceof BlockLiquid))
                        return false;
                    onLiquid = true;
                }
            }
        }
        return onLiquid;
    }

    public static boolean isMoving(Entity ent) {
        return ent == Minecraft.getMinecraft().thePlayer
                ? ((Minecraft.getMinecraft().thePlayer.moveForward != 0
                || Minecraft.getMinecraft().thePlayer.moveStrafing != 0))
                : (ent.motionX != 0 || ent.motionZ != 0);
    }

}
