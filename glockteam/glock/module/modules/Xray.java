package glockteam.glock.module.modules;

import glockteam.glock.module.Module;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 12/23/2016.
 */
public class Xray extends Module {

    public Xray() {
        super("Xray", Type.RENDER, 0xFFCECECE);
        this.setBind(Keyboard.KEY_X);
    }

    @Override
    public void onEnable() {
        mc.renderGlobal.loadRenderers();
        super.onEnable();
    }

    @Override
    public void onDisable() {
        mc.renderGlobal.loadRenderers();
        super.onEnable();
    }
}
