package glockteam.glock.module.modules;

import com.darkmagician6.eventapi.EventTarget;
import com.darkmagician6.eventapi.types.EventType;
import glockteam.glock.events.UpdateEvent;
import glockteam.glock.module.Module;
import org.lwjgl.input.Keyboard;

/**
 * Created by Kix on 12/22/2016.
 */
public class Sprint extends Module {

    public Sprint() {
        super("Sprint", Type.MOVEMENT, 0xFF00BFFF);
        this.setBind(Keyboard.KEY_LCONTROL);
    }

    private boolean canSuckASexyPeePee() {
        return !mc.thePlayer.isCollidedHorizontally && !mc.thePlayer.isSneaking()
                && mc.thePlayer.getFoodStats().getFoodLevel() > 5
                && (mc.thePlayer.movementInput.moveForward != 0.0f || mc.thePlayer.movementInput.moveStrafe != 0.0f);
    }

    @EventTarget
    public void onPlayerUpdate(UpdateEvent event) {
        if (event.type == EventType.POST) {
            if (canSuckASexyPeePee()) {
                mc.thePlayer.setSprinting(true);
            } else {
                mc.thePlayer.setSprinting(false);
            }
        }
    }

}
