package glockteam.glock.module;

import net.minecraft.client.Minecraft;

public class Mode<T> {

    protected T parent;
    private String name;
    protected Minecraft mc = Minecraft.getMinecraft();

    public Mode(String name, T parent) {
        this.parent = parent;
        this.name = name;
    }

    public void onEnable() {
    }

    public void onDisable() {
    }

    public T getParent() {
        return parent;
    }

    public String getName() {
        return name;
    }

}
