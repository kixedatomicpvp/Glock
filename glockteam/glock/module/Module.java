package glockteam.glock.module;

import com.darkmagician6.eventapi.EventManager;
import glockteam.glock.utils.file.FileUtils;
import glockteam.glock.utils.file.Value;
import glockteam.glock.utils.util.Reference;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kix on 12/22/2016.
 */
public class Module extends Reference {

    private String name;
    private int bind;
    private Type type;
    private boolean isEnabled;
    private boolean isHidden;
    private int color;
    private List<Mode> modes;
    private List<Value> values = new ArrayList();
    private Mode mode;

    public Module(String name, Type type, int color) {
        this.name = name;
        this.type = type;
        this.color = color;
        modes = new ArrayList<>();
    }

    public List<Value> getValues() {
        return values;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBind() {
        return bind;
    }

    public void setBind(int bind) {
        this.bind = bind;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            if (this.mode != null) {
                EventManager.register(this.mode);
                this.mode.onEnable();
            }
            this.isEnabled = true;
            onEnable();
        } else {
            if (this.mode != null) {
                EventManager.unregister(mode);
                mode.onDisable();
            }
            this.isEnabled = false;
            onDisable();
        }
    }

    public List<Mode> getModes() {
        return modes;
    }

    public void addMode(Mode mode) {
        modes.add(mode);
        if (this.mode == null) {
            this.mode = mode;
        }
    }

    public void setMode(Mode gay) {

        if (this.mode != null) {
            EventManager.unregister(mode);
            mode.onDisable();
        }

        if (isEnabled()) {
            EventManager.register(gay);
            gay.onEnable();
        }

        this.mode = gay;

    }

    public void toggle() {
        setEnabled(!isEnabled());
    }

    public void onEnable() {
        EventManager.register(this);
    }

    public void onDisable() {
        EventManager.unregister(this);
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public enum Type {
        COMBAT,
        MOVEMENT,
        MISCELLANEOUS,
        RENDER,
        WORLD,
        NONE
    }

}
