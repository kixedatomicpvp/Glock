package glockteam.glock.manager.managers;

import com.darkmagician6.eventapi.EventManager;
import glockteam.glock.command.Command;
import glockteam.glock.command.commands.KillAuraCommands;
import glockteam.glock.command.commands.Toggle;
import glockteam.glock.manager.Manager;
import glockteam.glock.module.Module;

/**
 * Created by Kix on 12/23/2016.
 */
public class CommandManager extends Manager<Command> {

    public CommandManager() {
        add(new Toggle());
        add(new KillAuraCommands.KillAuraPlayers());
        add(new KillAuraCommands.KillAuraInvisibles());
        add(new KillAuraCommands.KillAuraMobs());
        add(new KillAuraCommands.KillAuraAnimals());
        EventManager.register(this);
    }

    public void add(final Command content) {
        this.addContent(content);
    }

    public Command getCommandByName(String name) {
        if (getContents() == null)
            return null;
        for (final Command command : getContents()) {
            if (command.getName().equalsIgnoreCase(name))
                return command;
            for (final String alias : command.getAliases()) {
                if (alias.equalsIgnoreCase(name))
                    return command;
            }
        }
        return null;
    }

}
