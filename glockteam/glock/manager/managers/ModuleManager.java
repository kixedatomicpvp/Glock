package glockteam.glock.manager.managers;

import glockteam.glock.manager.Manager;
import glockteam.glock.module.Module;
import glockteam.glock.module.modules.*;

/**
 * Created by Kix on 12/22/2016.
 */
public class ModuleManager extends Manager<Module> {

    public ModuleManager() {
        add(new Overlay());
        add(new Killaura());
        add(new BowAimbot());
        add(new Sprint());
        add(new AutoSoup());
        add(new AntiVelocity());
        add(new Speed());
        add(new Xray());
        add(new Retard());
    }

    public void add(final Module content) {
        this.addContent(content);
    }

    public Module find(Class<? extends Module> clazz) {
        for (Module mod : getContents()) {
            if (mod.getClass() == clazz) {
                return mod;
            }
        }
        return null;
    }

    public Module find(String theMod) {
        for (Module mod : this.getContents()) {
            if (mod.getName().equalsIgnoreCase(theMod)) {
                return mod;
            }
        }
        return null;
    }

}
