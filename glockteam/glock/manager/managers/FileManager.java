package glockteam.glock.manager.managers;

import glockteam.glock.manager.Manager;
import glockteam.glock.utils.file.AbstractFile;

import java.io.IOException;

/**
 * Created by Kix on 12/22/2016.
 */
public class FileManager extends Manager<AbstractFile> {

    public void loadAllFiles() {
        for (AbstractFile file : getContents()) {
            try {
                file.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveAllFiles() {
        for (AbstractFile file : getContents()) {
            try {
                file.save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveFile(String fileName) {
        for (AbstractFile file : getContents()) {
            try {
                if (file.getName().equalsIgnoreCase(fileName)) {
                    file.save();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadFile(String fileName) {
        for (AbstractFile file : getContents()) {
            try {
                if (file.getName().equalsIgnoreCase(fileName)) {
                    file.load();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
