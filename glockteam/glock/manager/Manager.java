package glockteam.glock.manager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kix on 12/22/2016.
 */
public class Manager<T> {

    public  List<T> contents;

    public Manager() {
        this.contents = new ArrayList<T>();
    }

    public List<T> getContents() {
        return this.contents;
    }

    public void addContent(final T content) {
        this.contents.add(content);
    }

    public void removeContent(final T content) {
        this.contents.remove(content);
    }

    public boolean hasContent(final T content) {
        return this.contents.contains(content);
    }


}
