package glockteam.glock;

import glockteam.glock.manager.managers.CommandManager;
import glockteam.glock.manager.managers.ModuleManager;
import glockteam.glock.utils.util.Reference;

/**
 * Created by Kix on 12/22/2016.
 */
public class Glock extends Reference {
    /**
     * This constructor is called in the Minecraft Startup void so you can start using your modifications.
     */
    public Glock() {
        commandManager = new CommandManager();
        moduleManager = new ModuleManager();
    }
}
