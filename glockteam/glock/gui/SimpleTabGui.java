package glockteam.glock.gui;

import glockteam.glock.Glock;
import glockteam.glock.module.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kix on 12/23/2016.
 */
public class SimpleTabGui {

    private static int selected, moduleSelected;
    private static boolean isOpen;
    private static final Minecraft mc = Minecraft.getMinecraft();

    public static void drawTabGui() {

        for (int i = 0; i < Module.Type.values().length; i++) {
            Module.Type cat = Module.Type.values()[i];

            if (cat == Module.Type.NONE)
                continue;

            int y = 12;

            Gui.drawRect(2, y + i * 10, 75, y + 10 + i * 10, selected == i ? new Color(204, 204, 204, 255).getRGB() : new Color(0, 0, 0, 150).getRGB());
            String name = cat.name().substring(0, 1) + cat.name().substring(1).toLowerCase();

            if(selected == i) {
                mc.fontRendererObj.drawStringWithShadow(" " + name, 4, y + i * 10 + 1, 0xFFFFFF);
            }else {
                mc.fontRendererObj.drawStringWithShadow(name, 4, y + i * 10 + 1, new Color(195, 195, 195, 255).getRGB());
            }

            if (isOpen && i == selected) {
                for (int j = 0; j < getModsForCategory(cat).size(); j++) {
                    Module mod = getModsForCategory(cat).get(j);

                    Gui.drawRect(77, y + y + i * 10 + j * 10 - 2, 165, y + i * 10 + j * 10, moduleSelected == j ? 0xCCCCCCCC : new Color(0, 0, 0, 150).getRGB());
                    if(moduleSelected == j) {
                        mc.fontRendererObj.drawStringWithShadow(" " + mod.getName(), 79, y + i * 10 + j * 10 + 1, mod.isEnabled() ? new Color(255, 203, 56, 255).getRGB() : new Color(255, 255, 255, 255).getRGB());
                    }else{
                        mc.fontRendererObj.drawStringWithShadow(mod.getName(), 79, y + i * 10 + j * 10 + 1, mod.isEnabled() ? new Color(255, 203, 56, 255).getRGB() : new Color(195, 195, 195, 255).getRGB());
                    }
                }
            }

        }
    }


    public static void onKey(int key) {
        if (key == Keyboard.KEY_UP) {
            if (!isOpen) {
                selected--;
                if (selected <= 0) {
                    selected = 0;
                }
            } else {
                moduleSelected--;
                if (moduleSelected <= 0) {
                    moduleSelected = 0;
                }
            }
        }

        if (key == Keyboard.KEY_DOWN) {
            if (!isOpen) {
                selected++;
                if (selected >= Module.Type.values().length - 2) {
                    selected = Module.Type.values().length - 2;
                }

            } else {
                moduleSelected++;
                if (moduleSelected >= getModsForCategory(Module.Type.values()[selected]).size() -1) {
                    moduleSelected = getModsForCategory(Module.Type.values()[selected]).size() -1;
                }
            }
        }

        if(key == Keyboard.KEY_RIGHT){
            isOpen = true;
        }

        if(key == Keyboard.KEY_LEFT){
            isOpen = false;
            moduleSelected = 0;
        }

        if(key == Keyboard.KEY_RETURN){
            getModsForCategory(Module.Type.values()[selected]).get(moduleSelected).toggle();
        }

    }

    private static List<Module> getModsForCategory(Module.Type category) {
        List<Module> mods = new ArrayList<>();

        for (Module mod : Glock.moduleManager.getContents()) {
            if (mod.getType() == category) {
                mods.add(mod);
            }
        }

        return mods;

    }

}
