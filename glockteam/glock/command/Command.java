package glockteam.glock.command;

import glockteam.glock.utils.util.ChatUtil;

/**
 * Created by Kix on 12/23/2016.
 */
public abstract class Command {

    private String name;
    private String purpose;
    private String syntax;
    private String[] aliases;

    public Command(String name, String purpose, String syntax, String... aliases) {
        this.name = name;
        this.purpose = purpose;
        this.syntax = syntax;
        this.aliases = aliases;
    }

    public String[] getAliases() {
        return aliases;
    }

    public String getName() {
        return name;
    }

    public String getPurpose() {
        return purpose;
    }

    public String getSyntax() {
        return syntax;
    }

    public void syntaxError(){
        ChatUtil.printLn("Incorrect Usage! Syntax: " + this.syntax);
    }

    public abstract void send(final String args);

}
