package glockteam.glock.command.commands;

import glockteam.glock.command.Command;
import glockteam.glock.module.modules.Killaura;
import glockteam.glock.utils.util.ChatUtil;

/**
 * Created by Kix on 12/24/2016.
 */
public class KillAuraCommands {

    public static class KillAuraPlayers extends Command{
        public KillAuraPlayers() {
            super("killauraplayers", "allows the player to stop attacking players or start attacking players", "", "kap");
        }

        @Override
        public void send(String args) {
            Killaura.players.setValue(!Killaura.players.getValue());
            ChatUtil.printLn("Kill Aura will now " + (Killaura.players.getValue() ? "focus " : "not focus ") + "on players.");
        }
    }

    public static class KillAuraInvisibles extends Command{
        public KillAuraInvisibles() {
            super("killaurainvisibles", "allows the player to stop attacking players or start attacking players", "", "kai");
        }

        @Override
        public void send(String args) {
            Killaura.invisibles.setValue(!Killaura.invisibles.getValue());
            ChatUtil.printLn("Kill Aura will now " + (Killaura.invisibles.getValue() ? "focus " : "not focus ") + "on invisibles.");
        }
    }

    public static class KillAuraMobs extends Command{
        public KillAuraMobs() {
            super("killauramobs", "allows the player to stop attacking players or start attacking players", "", "kam");
        }

        @Override
        public void send(String args) {
            Killaura.mobs.setValue(!Killaura.mobs.getValue());
            ChatUtil.printLn("Kill Aura will now " + (Killaura.mobs.getValue() ? "focus " : "not focus ") + "on mobs.");
        }
    }

    public static class KillAuraAnimals extends Command{
        public KillAuraAnimals() {
            super("killauraanimals", "allows the player to stop attacking players or start attacking players", "", "kaa");
        }

        @Override
        public void send(String args) {
            Killaura.animals.setValue(!Killaura.animals.getValue());
            ChatUtil.printLn("Kill Aura will now " + (Killaura.animals.getValue() ? "focus " : "not focus ") + "on animals.");
        }
    }

}
