package glockteam.glock.command.commands;

import glockteam.glock.Glock;
import glockteam.glock.command.Command;
import glockteam.glock.module.Module;
import glockteam.glock.utils.util.ChatUtil;

/**
 * Created by Kix on 12/23/2016.
 */
public class Toggle extends Command {

    public Toggle() {
        super("toggle", "Allows the user to toggle a module", "<mod name>", "t");
    }

    @Override
    public void send(String args) {
        final String[] arguments = args.split(" ");
        final Module mod = Glock.moduleManager.find(arguments[1]);
        if (mod == null) {
            ChatUtil.printLn("Module \"" + arguments[1] + "\" was not found!");
        } else {
            mod.toggle();
            ChatUtil.printLn("Module \"" + mod.getName() + "\" was toggled "
                    + (mod.isEnabled() ? "\2477on" : "\2477off") + "\247f.");
        }
    }
}
