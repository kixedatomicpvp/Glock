package glockteam.glock.utils.util;

import java.awt.Color;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;

public class RenderUtil {

	public static void drawTexturedModalRect(int x, int y, int textureX, int textureY, int width, int height) {
        float var7 = 0.00390625F;
        float var8 = 0.00390625F;
        Tessellator var9 = Tessellator.getInstance();
        WorldRenderer var10 = var9.getWorldRenderer();
        var10.startDrawingQuads();
        var10.addVertexWithUV((double) (x + 0), (double) (y + height), 0.0D, (double) ((float) (textureX + 0) * var7), (double) ((float) (textureY + height) * var8));
        var10.addVertexWithUV((double) (x + width), (double) (y + height), 0.0D, (double) ((float) (textureX + width) * var7), (double) ((float) (textureY + height) * var8));
        var10.addVertexWithUV((double) (x + width), (double) (y + 0), 0.0D, (double) ((float) (textureX + width) * var7), (double) ((float) (textureY + 0) * var8));
        var10.addVertexWithUV((double) (x + 0), (double) (y + 0), 0.0D, (double) ((float) (textureX + 0) * var7), (double) ((float) (textureY + 0) * var8));
        var9.draw();
    }
//
//	public static void drawItem(final ItemStack item, final int posX, final int posY) {
//		final RenderItem itemRenderer = Minecraft.getMinecraft().getRenderItem();
//		itemRenderer.func_175042_a(item, posX, posY);
//		itemRenderer.renderItemOverlayIntoGUI(Minecraft.getMinecraft().fontRendererObj, item, posX, posY);
//		GlStateManager.disableCull();
//		GlStateManager.enableAlpha();
//		GlStateManager.disableBlend();
//		GlStateManager.disableLighting();
//		GlStateManager.disableCull();
//		GlStateManager.clear(256);
//	}
	
	public static void enableGL3D(float lineWidth)
	{
		GL11.glDisable(3008);
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glEnable(2884);
		GL11.glEnable(2848);
		GL11.glHint(3154, 4354);
		GL11.glHint(3155, 4354);
		GL11.glLineWidth(lineWidth);
	}

	public static void disableGL3D()
	{
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDisable(3042);
		GL11.glEnable(3008);
		GL11.glDepthMask(true);
		GL11.glCullFace(1029);
		GL11.glDisable(2848);
		GL11.glHint(3154, 4352);
		GL11.glHint(3155, 4352);
	}

    public static void drawTri(final double x1, final double y1, final double x2, final double y2, final double x3, final double y3, final double width, final Color c) {
        GlStateManager.pushMatrix();
        GL11.glEnable(3042);
        GL11.glDisable(3553);
        GL11.glEnable(2848);
        GL11.glBlendFunc(770, 771);
        setColor(c);
        GL11.glLineWidth((float)width);
        GL11.glBegin(3);
        GL11.glVertex2d(x1, y1);
        GL11.glVertex2d(x2, y2);
        GL11.glVertex2d(x3, y3);
        GL11.glEnd();
        GlStateManager.popMatrix();
    }
	
    public static void setColor(final Color c) {
        GL11.glColor4d(c.getRed() / 255.0f, c.getGreen() / 255.0f, c.getBlue() / 255.0f, c.getAlpha() / 255.0f);
    }
    
	public static int getMouseX() {
    	return (int) ((Mouse.getX() * getScreenWidth() / Minecraft.getMinecraft().displayWidth));
    }
	
	public static int getMouseY() {
    	return (int) ((getScreenHeight() - Mouse.getY() * getScreenHeight() / Minecraft.getMinecraft().displayHeight - 1));
    }
    
    public static int getScreenWidth() {
    	return Minecraft.getMinecraft().displayWidth / getScaleFactor();
    }

    public static int getScreenHeight() {
    	return Minecraft.getMinecraft().displayHeight / getScaleFactor();
    }
    
    public static int getScaleFactor() {
		int scaleFactor = 1;
		boolean isUnicode = Minecraft.getMinecraft().isUnicode();
		int scaleSetting = Minecraft.getMinecraft().gameSettings.guiScale;

		if (scaleSetting == 0) {
			scaleSetting = 1000;
		}
		while (scaleFactor < scaleSetting && Minecraft.getMinecraft().displayWidth / (scaleFactor + 1) >= 320 && Minecraft.getMinecraft().displayHeight / (scaleFactor + 1) >= 240) {
			scaleFactor++;
		}

		if (isUnicode && scaleFactor % 2 != 0 && scaleFactor != 1) {
			scaleFactor--;
		}
		return scaleFactor;
	}
	
	public static void drawHLine(float x, float x2, float y, int par4) {
		if (x2 < x) {
			float var5 = x;
			x = x2;
			x2 = var5;
		}
		drawRect(x, y, x2 + 1, y + 1, par4);
	}

	public static void drawVLine(float x, float y, float y2, int par4) {
		if (y2 < y) {
			float var5 = y;
			y = y2;
			y2 = var5;
		}

		drawRect(x, y + 1, x + 1, y2, par4);
	}

	public static void drawRoundedRect(float x, float y, float x1, float y1, int insideC) {
		x *= 2;
		y *= 2;
		x1 *= 2;
		y1 *= 2;
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawVLine(x, y + 1, y1 - 2, insideC);
		drawVLine(x1 - 1, y + 1, y1 - 2, insideC);
		drawHLine(x + 2, x1 - 3, y, insideC);
		drawHLine(x + 2, x1 - 3, y1 - 1, insideC);
		drawHLine(x + 1, x + 1, y + 1, insideC);
		drawHLine(x1 - 2, x1 - 2, y + 1, insideC);
		drawHLine(x1 - 2, x1 - 2, y1 - 2, insideC);
		drawHLine(x + 1, x + 1, y1 - 2, insideC);
		GL11.glPushMatrix();
		drawRect(x + 1, y + 1, x1 - 1, y1 - 1, insideC);
		GL11.glScalef(2.0F, 2.0F, 2.0F);
		GL11.glPopMatrix();
		GL11.glPopMatrix();
	}

	public static void drawBorderRect(int left, int top, int right, int bottom, int bcolor, int icolor, int bwidth) {
		Gui.drawRect(left + bwidth, top + bwidth, right - bwidth, bottom - bwidth, icolor);
		Gui.drawRect(left, top, left + bwidth, bottom, bcolor);
		Gui.drawRect(left + bwidth, top, right, top + bwidth, bcolor);
		Gui.drawRect(left + bwidth, bottom - bwidth, right, bottom, bcolor);
		Gui.drawRect(right - bwidth, top + bwidth, right, bottom - bwidth, bcolor);
	}

	public static int withAlpha(int c, float a) {
		float r = (c >> 16 & 255) / 255F;
		float g = (c >> 8 & 255) / 255F;
		float b = (c & 255) / 255F;
		return new Color(r, g, b, a).getRGB();
	}

	public static void drawRect(double x, double y, double x1, double y1, int color) {
		Gui.drawRect(x, y, x1, y1, color);
	}

	public static void drawGradientRect(int left, int top, int right, int bottom, int startColor, int endColor) {
		float var7 = (float) (startColor >> 24 & 255) / 255.0F;
		float var8 = (float) (startColor >> 16 & 255) / 255.0F;
		float var9 = (float) (startColor >> 8 & 255) / 255.0F;
		float var10 = (float) (startColor & 255) / 255.0F;
		float var11 = (float) (endColor >> 24 & 255) / 255.0F;
		float var12 = (float) (endColor >> 16 & 255) / 255.0F;
		float var13 = (float) (endColor >> 8 & 255) / 255.0F;
		float var14 = (float) (endColor & 255) / 255.0F;
		GlStateManager.func_179090_x();
		GlStateManager.enableBlend();
		GlStateManager.disableAlpha();
		GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
		GlStateManager.shadeModel(7425);
		Tessellator var15 = Tessellator.getInstance();
		WorldRenderer var16 = var15.getWorldRenderer();
		var16.startDrawingQuads();
		var16.func_178960_a(var8, var9, var10, var7);
		var16.addVertex((double) right, (double) top, (double) 0);
		var16.addVertex((double) left, (double) top, (double) 0);
		var16.func_178960_a(var12, var13, var14, var11);
		var16.addVertex((double) left, (double) bottom, (double) 0);
		var16.addVertex((double) right, (double) bottom, (double) 0);
		var15.draw();
		GlStateManager.shadeModel(7424);
		GlStateManager.disableBlend();
		GlStateManager.enableAlpha();
		GlStateManager.func_179098_w();
	}

	 public static void drawBorderedRect(double left, double top, double right, double bottom, float borderWidth, int borderColor, int color) {
	        float alpha = (borderColor >> 24 & 0xFF) / 255.0f;
	        float red = (borderColor >> 16 & 0xFF) / 255.0f;
	        float green = (borderColor >> 8 & 0xFF) / 255.0f;
	        float blue = (borderColor & 0xFF) / 255.0f;
	        GlStateManager.pushMatrix();
	        drawRects(left, top, right, bottom, color);
	        GlStateManager.enableBlend();
	        GlStateManager.func_179090_x();
	        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
	        GlStateManager.color(red, green, blue, alpha);

	        if (borderWidth == 1.0F) {
	            GL11.glEnable(GL11.GL_LINE_SMOOTH);
	        }

	        GL11.glLineWidth(borderWidth);
	        Tessellator tessellator = Tessellator.getInstance();
	        WorldRenderer worldRenderer = tessellator.getWorldRenderer();
	        worldRenderer.startDrawing(1);
	        worldRenderer.addVertex(left, top, 0.0F);
	        worldRenderer.addVertex(left, bottom, 0.0F);
	        worldRenderer.addVertex(right, bottom, 0.0F);
	        worldRenderer.addVertex(right, top, 0.0F);
	        worldRenderer.addVertex(left, top, 0.0F);
	        worldRenderer.addVertex(right, top, 0.0F);
	        worldRenderer.addVertex(left, bottom, 0.0F);
	        worldRenderer.addVertex(right, bottom, 0.0F);
	        tessellator.draw();
	        GL11.glLineWidth(2.0F);

	        if (borderWidth == 1.0F) {
	            GL11.glDisable(GL11.GL_LINE_SMOOTH);
	        }

	        GlStateManager.func_179098_w();
	        GlStateManager.disableBlend();
	        GlStateManager.popMatrix();
	    }
	
	 public static void drawRects(double left, double top, double right, double bottom, int color) {
	        float alpha = (float) (color >> 24 & 255) / 255.0F;
	        float red = (float) (color >> 16 & 255) / 255.0F;
	        float green = (float) (color >> 8 & 255) / 255.0F;
	        float blue = (float) (color & 255) / 255.0F;
	        Tessellator var9 = Tessellator.getInstance();
	        WorldRenderer var10 = var9.getWorldRenderer();
	        GlStateManager.enableBlend();
	        GlStateManager.func_179090_x();
	        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
	        GlStateManager.color(red, green, blue, alpha);
	        var10.startDrawingQuads();
	        var10.addVertex(left, bottom, 0.0D);
	        var10.addVertex(right, bottom, 0.0D);
	        var10.addVertex(right, top, 0.0D);
	        var10.addVertex(left, top, 0.0D);
	        var9.draw();
	        GlStateManager.func_179098_w();
	        GlStateManager.disableBlend();
	    }
	 
	 public static void drawBorderedGradientRect(double left, double top, double right, double bottom, float borderWidth, int borderColor, int startColor, int endColor) {
	        float alpha = (borderColor >> 24 & 0xFF) / 255.0f;
	        float red = (borderColor >> 16 & 0xFF) / 255.0f;
	        float green = (borderColor >> 8 & 0xFF) / 255.0f;
	        float blue = (borderColor & 0xFF) / 255.0f;
	        GlStateManager.pushMatrix();
	        drawGradientRect(left, top, right, bottom, startColor, endColor);
	        GlStateManager.enableBlend();
	        GlStateManager.func_179090_x();
	        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
	        GlStateManager.color(red, green, blue, alpha);

	        if (borderWidth == 1.0F) {
	            GL11.glEnable(GL11.GL_LINE_SMOOTH);
	        }

	        GL11.glLineWidth(borderWidth);
	        Tessellator tessellator = Tessellator.getInstance();
	        WorldRenderer worldRenderer = tessellator.getWorldRenderer();
	        worldRenderer.startDrawing(1);
	        worldRenderer.addVertex(left, top, 0.0F);
	        worldRenderer.addVertex(left, bottom, 0.0F);
	        worldRenderer.addVertex(right, bottom, 0.0F);
	        worldRenderer.addVertex(right, top, 0.0F);
	        worldRenderer.addVertex(left, top, 0.0F);
	        worldRenderer.addVertex(right, top, 0.0F);
	        worldRenderer.addVertex(left, bottom, 0.0F);
	        worldRenderer.addVertex(right, bottom, 0.0F);
	        tessellator.draw();
	        GL11.glLineWidth(2.0F);

	        if (borderWidth == 1.0F) {
	            GL11.glDisable(GL11.GL_LINE_SMOOTH);
	        }

	        GlStateManager.func_179098_w();
	        GlStateManager.disableBlend();
	        GlStateManager.popMatrix();
	    }
	 
	 public static void drawGradientRect(double left, double top, double right, double bottom, int startColor, int endColor) {
	        float var7 = (float) (startColor >> 24 & 255) / 255.0F;
	        float var8 = (float) (startColor >> 16 & 255) / 255.0F;
	        float var9 = (float) (startColor >> 8 & 255) / 255.0F;
	        float var10 = (float) (startColor & 255) / 255.0F;
	        float var11 = (float) (endColor >> 24 & 255) / 255.0F;
	        float var12 = (float) (endColor >> 16 & 255) / 255.0F;
	        float var13 = (float) (endColor >> 8 & 255) / 255.0F;
	        float var14 = (float) (endColor & 255) / 255.0F;
	        GlStateManager.func_179090_x();
	        GlStateManager.enableBlend();
	        GlStateManager.disableAlpha();
	        GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
	        GlStateManager.shadeModel(7425);
	        Tessellator var15 = Tessellator.getInstance();
	        WorldRenderer var16 = var15.getWorldRenderer();
	        var16.startDrawingQuads();
	        var16.func_178960_a(var8, var9, var10, var7);
	        var16.addVertex(right, top, 0);
	        var16.addVertex(left, top, 0);
	        var16.func_178960_a(var12, var13, var14, var11);
	        var16.addVertex(left, bottom, 0);
	        var16.addVertex(right, bottom, 0);
	        var15.draw();
	        GlStateManager.shadeModel(7424);
	        GlStateManager.disableBlend();
	        GlStateManager.enableAlpha();
	        GlStateManager.func_179098_w();
	    }
	 
	    public static void drawBorderedRect(double left, double top, double right, double bottom, int borderColor, int color) {
	        drawBorderedRect(left, top, right, bottom, 1.0F, borderColor, color);
	    }
	    
	    public static void drawBorderedGradientRect(double left, double top, double right, double bottom, int borderColor, int startColor, int endColor) {
	        drawBorderedGradientRect(left, top, right, bottom, 1.0F, borderColor, startColor, endColor);
	    }
	 
	public static void drawOutlinedBoundingBox(AxisAlignedBB aa) {
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldRenderer = tessellator.getWorldRenderer();
		worldRenderer.startDrawing(3);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		tessellator.draw();
		worldRenderer.startDrawing(3);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		tessellator.draw();
		worldRenderer.startDrawing(1);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		tessellator.draw();
	}

	public static void drawBoundingBox(AxisAlignedBB aa) {
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldRenderer = tessellator.getWorldRenderer();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.maxZ);
		worldRenderer.addVertex(aa.minX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.minX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.minZ);
		worldRenderer.addVertex(aa.maxX, aa.maxY, aa.maxZ);
		worldRenderer.addVertex(aa.maxX, aa.minY, aa.maxZ);
		tessellator.draw();
	}

	public static void drawOutlinedBlockESP(double x, double y, double z, float red, float green, float blue,
			float alpha, float lineWidth) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glLineWidth(lineWidth);
		GL11.glColor4f(red, green, blue, alpha);
		drawOutlinedBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawBlockESP(double x, double y, double z, float red, float green, float blue, float alpha,
			float lineRed, float lineGreen, float lineBlue, float lineAlpha, float lineWidth) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glLineWidth(lineWidth);
		GL11.glColor4f(lineRed, lineGreen, lineBlue, lineAlpha);
		drawLines(new AxisAlignedBB(x, y, z, x + 1.0, y + 1.0, z + 1.0));
		drawOutlinedBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}




	private void drawBoxes(final AxisAlignedBB bb, final float[] color) {
		GlStateManager.color(color[0], color[1], color[2], 0.6f);
		RenderUtil.drawLines(bb);
		RenderGlobal.drawOutlinedBoundingBox(bb, -1);
		GlStateManager.color(color[0], color[1], color[2], 0.11f);
		RenderUtil.drawFilledBox(bb);
	}

	public static void drawFilledBox(final AxisAlignedBB bb) {
		final Tessellator tessellator = Tessellator.getInstance();
		final WorldRenderer worldRenderer = tessellator.getWorldRenderer();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		tessellator.draw();
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.maxZ);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		tessellator.draw();
	}

	public static void drawClickTPESP(double x, double y, double z, float red, float green, float blue, float alpha,
			float lineRed, float lineGreen, float lineBlue, float lineAlpha, float lineWidth) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x, y + 1.1, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glLineWidth(lineWidth);
		GL11.glColor4f(lineRed, lineGreen, lineBlue, lineAlpha);
		drawOutlinedBoundingBox(new AxisAlignedBB(x, y + 1.1, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawLines(final AxisAlignedBB bb) {
		final Tessellator tessellator = Tessellator.getInstance();
		final WorldRenderer worldRenderer = tessellator.getWorldRenderer();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.maxZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.minZ);
		tessellator.draw();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.maxX, bb.minY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.minY, bb.maxZ);
		tessellator.draw();
		worldRenderer.startDrawing(2);
		worldRenderer.addVertex(bb.maxX, bb.maxY, bb.minZ);
		worldRenderer.addVertex(bb.minX, bb.maxY, bb.maxZ);
		tessellator.draw();
	}

	public static void drawSolidBlockESP(double x, double y, double z, float red, float green, float blue,
			float alpha) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x, y, z, x + 1.0, y + 1.0, z + 1.0));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawOutlinedEntityESP(double x, double y, double z, double width, double height, float red,
			float green, float blue, float alpha) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawOutlinedBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawSolidEntityESP(double x, double y, double z, double width, double height, float red,
			float green, float blue, float alpha) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawEntityESP(double x, double y, double z, double width, double height, float red, float green,
			float blue, float alpha, float lineRed, float lineGreen, float lineBlue, float lineAlpha, float lineWdith) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glLineWidth(lineWdith);
		GL11.glColor4f(lineRed, lineGreen, lineBlue, lineAlpha);
		drawLines(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		drawOutlinedBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}

	public static void drawEntityESPWithoutLines(double x, double y, double z, double width, double height, float red, float green,
			float blue, float alpha, float lineRed, float lineGreen, float lineBlue, float lineAlpha, float lineWdith) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glColor4f(red, green, blue, alpha);
		drawBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glLineWidth(lineWdith);
		GL11.glColor4f(lineRed, lineGreen, lineBlue, lineAlpha);
		drawOutlinedBoundingBox(new AxisAlignedBB(x - width, y, z - width, x + width, y + height, z + width));
		GL11.glDisable(2848);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDepthMask(true);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}
	
	public static void drawTracerLine(double[] pos, float[] c, float width) {
		GL11.glPushMatrix();
		GL11.glEnable(3042);
		GL11.glEnable(2848);
		GL11.glDisable(2929);
		GL11.glDisable(3553);
		GL11.glBlendFunc(770, 771);
		GL11.glEnable(3042);
		GL11.glLineWidth(width);
		GL11.glColor4f(c[0], c[1], c[2], c[3]);
		GL11.glBegin(GL11.GL_LINES);
		{
			GL11.glVertex3d(0, Minecraft.getMinecraft().thePlayer.getEyeHeight(), 0);
			GL11.glVertex3d(pos[0], pos[1], pos[2]);
		}
		GL11.glEnd();
		GL11.glBegin(GL11.GL_LINES);
		{
			GL11.glVertex3d(pos[0], pos[1] + 2, pos[2]);
			GL11.glVertex3d(pos[0], pos[1], pos[2]);
		}
		GL11.glEnd();
		GL11.glDisable(3042);
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDisable(2848);
		GL11.glDisable(3042);
		GL11.glPopMatrix();
	}


	public static void dr(double i, double j, double k, double l, int i1) {
		if (i < k) {
			double j2 = i;
			i = k;
			k = j2;
		}
		if (j < l) {
			double k2 = j;
			j = l;
			l = k2;
		}
		float f = (i1 >> 24 & 0xFF) / 255.0f;
		float f2 = (i1 >> 16 & 0xFF) / 255.0f;
		float f3 = (i1 >> 8 & 0xFF) / 255.0f;
		float f4 = (i1 & 0xFF) / 255.0f;
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldRenderer = tessellator.getWorldRenderer();
		GL11.glEnable(3042);
		GL11.glDisable(3553);
		GL11.glBlendFunc(770, 771);
		GL11.glColor4f(f2, f3, f4, f);
		worldRenderer.startDrawingQuads();
		worldRenderer.addVertex(i, l, 0.0);
		worldRenderer.addVertex(k, l, 0.0);
		worldRenderer.addVertex(k, j, 0.0);
		worldRenderer.addVertex(i, j, 0.0);
		tessellator.draw();
		GL11.glEnable(3553);
		GL11.glDisable(3042);
	}
	
	
}
