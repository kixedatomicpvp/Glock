package glockteam.glock.utils.util;

import net.minecraft.client.Minecraft;
import org.lwjgl.opengl.GL11;

import java.awt.*;

public class ColorUtil {

	private static final Minecraft mc = Minecraft.getMinecraft();

	public static int randomColor() {
		return 0xFF << 24 | (int) (Math.random() * 0xFFFFFF);
	}

	public static int transparency(int color, double alpha) {
		if ((color & -67108864) == 0)
			color |= -16777216;

		Color c = new Color(color);
		float r = (1f / 255f) * c.getRed();
		float g = (1f / 255f) * c.getGreen();
		float b = (1f / 255f) * c.getBlue();

		return new Color(r, g, b, (float) alpha).getRGB();
	}

	public static int transparency(Color color, double alpha) {
		if (alpha > 1)
			alpha = 1;
		else if (alpha < 0)
			alpha = 0;

		return new Color(color.getRed(), color.getGreen(), color.getBlue(), (float) alpha).getRGB();
	}

	public static Color rainbow(long offset, float fade) {
		float hue = (System.nanoTime() + offset) / 10000000000f % 1;
		long color = Long.parseLong(Integer.toHexString(Integer.valueOf(Color.HSBtoRGB(hue, 1f, 1f))), 16);
		Color c = new Color((int) color);
		return new Color((c.getRed() / 255f) * fade, (c.getGreen() / 255f) * fade, (c.getBlue() / 255f) * fade,
				c.getAlpha() / 255f);
	}

	public static float[] getRGBA(int color) {
		if ((color & -67108864) == 0) {
			color |= -16777216;
		}

		float a = (color >> 24 & 255) / 255f;
		float r = (color >> 16 & 255) / 255f;
		float g = (color >> 8 & 255) / 255f;
		float b = (color & 255) / 255f;
		return new float[] { r, g, b, a };
	}

	public static void glColor(int color) {
		float[] colors = getRGBA(color);
		GL11.glColor4f(colors[0], colors[1], colors[2], colors[3]);
	}

	public static int intFromHex(String hex) {
		try {
			return Integer.parseInt(hex, 16);
		} catch (NumberFormatException e) {
			return 0xFFFFFFFF;
		}
	}

	public static String hexFromInt(int color) {
		if ((color & -67108864) == 0)
			color |= -16777216;

		return hexFromColor(new Color(color));
	}

	public static String hexFromColor(Color color) {
		return Integer.toHexString(color.getRGB()).substring(2);
	}

	public static int intFromColor(Color color) {
		return intFromHex(hexFromColor(color));
	}

	public static Color blend(Color color1, Color color2, double ratio) {
		float r = (float) ratio;
		float ir = (float) 1.0 - r;

		float rgb1[] = new float[3];
		float rgb2[] = new float[3];

		color1.getColorComponents(rgb1);
		color2.getColorComponents(rgb2);

		Color color = new Color(rgb1[0] * r + rgb2[0] * ir, rgb1[1] * r + rgb2[1] * ir, rgb1[2] * r + rgb2[2] * ir);

		return color;
	}

	public static Color blend(Color color1, Color color2) {
		return blend(color1, color2, 0.5);
	}

	public static Color darken(Color color, double fraction) {
		int red = (int) Math.round(color.getRed() * (1.0 - fraction));
		int green = (int) Math.round(color.getGreen() * (1.0 - fraction));
		int blue = (int) Math.round(color.getBlue() * (1.0 - fraction));

		if (red < 0)
			red = 0;
		else if (red > 255)
			red = 255;
		if (green < 0)
			green = 0;
		else if (green > 255)
			green = 255;
		if (blue < 0)
			blue = 0;
		else if (blue > 255)
			blue = 255;

		int alpha = color.getAlpha();

		return new Color(red, green, blue, alpha);
	}

	public static Color lighten(Color color, double fraction) {
		int red = (int) Math.round(color.getRed() * (1.0 + fraction));
		int green = (int) Math.round(color.getGreen() * (1.0 + fraction));
		int blue = (int) Math.round(color.getBlue() * (1.0 + fraction));

		if (red < 0)
			red = 0;
		else if (red > 255)
			red = 255;

		if (green < 0)
			green = 0;
		else if (green > 255)
			green = 255;

		if (blue < 0)
			blue = 0;
		else if (blue > 255)
			blue = 255;

		int alpha = color.getAlpha();

		return new Color(red, green, blue, alpha);
	}

	public static int getColor(char character) {
		return mc.fontRendererObj.func_175064_b(character);
	}
}
