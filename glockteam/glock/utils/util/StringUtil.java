package glockteam.glock.utils.util;

import java.util.regex.Pattern;

public class StringUtil {

    private static final Pattern patternControlCode = Pattern.compile("(?i)\\u00A7[0-9A-FK-OR]");
    private static final Pattern patternColorCode = Pattern.compile("(?i)\\u00A7[0-9A-F]");
    private static final Pattern patternFormatCode = Pattern.compile("(?i)\\u00A7[K-O]");

    public static String stripControlCodes(String s) {
        return patternControlCode.matcher(s).replaceAll("");
    }

    public static String stripColorCodes(String s) {
        return patternColorCode.matcher(s).replaceAll("");
    }

    public static String stripFormatCodes(String s) {
        return patternFormatCode.matcher(s).replaceAll("");
    }
	
}
