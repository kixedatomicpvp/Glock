package glockteam.glock.utils.util;

import glockteam.glock.utils.AbstractUtil;
import net.minecraft.util.ChatComponentText;

/**
 * Created by Kix on 12/23/2016.
 */
public class ChatUtil extends AbstractUtil{

    private static String prefix = "\2477[Console] \2477";

    public static void printLn(String message) {
        if (mc.thePlayer != null) {
            mc.thePlayer.addChatComponentMessage(new ChatComponentText(prefix + message));
        }
    }

}
