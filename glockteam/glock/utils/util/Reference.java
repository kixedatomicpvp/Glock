package glockteam.glock.utils.util;
import glockteam.glock.manager.managers.CommandManager;
import glockteam.glock.manager.managers.FileManager;
import glockteam.glock.manager.managers.ModuleManager;
import glockteam.glock.manager.managers.ValueManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;

/**
 * Created by Kix on 12/22/2016.
 */
public class Reference {

    /**
     * This class is a class that can be called at any time. Mainly used for calling the name of the client and the version along with other information.
     **/
    public static String tag = "Glock";
    public static Double version = 1.0;
    public static Minecraft mc = Minecraft.getMinecraft();
    public static EntityPlayerSP p = Minecraft.getMinecraft().thePlayer;
    public static FontRenderer fr = Minecraft.getMinecraft().fontRendererObj;
    public static ModuleManager moduleManager = new ModuleManager();
    public static CommandManager commandManager = new CommandManager();
    public static ValueManager valueManager = new ValueManager();
    public static FileManager fileManager = new FileManager();
    public static KillAuraUtils ka = new KillAuraUtils();

}
