package glockteam.glock.utils.util;

import glockteam.glock.utils.AbstractUtil;

/**
 * Created by Kix on 12/22/2016.
 */
public class TimerUtil extends AbstractUtil {

    private long lastTime = -1L;

    public long getLast() {
        return lastTime;
    }

    public boolean hasReached(long time) {
        return getTime() >= time;
    }

    public static long getCurrent() {
        return (System.nanoTime() / 1000000L);
    }

    public long getTime() {
        return getCurrent() - getLast();
    }

    public void reset() {
        this.lastTime = getCurrent();
    }

}
