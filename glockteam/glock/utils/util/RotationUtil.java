package glockteam.glock.utils.util;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

/**
 * Created by Kix on 10/14/2016.
 */
public class RotationUtil {

    public static float[] getRotation(final Entity ent) {
        final double x = ent.posX;
        final double z = ent.posZ;
        final double y = ent.boundingBox.maxY;
        return getRotationFromPosition(x, z, y);
    }

    public static float[] getRotations(final Entity entity) {
        final double positionX = entity.posX - Minecraft.getMinecraft().thePlayer.posX;
        final double positionZ = entity.posZ - Minecraft.getMinecraft().thePlayer.posZ;
        final double positionY = entity.posY + entity.getEyeHeight() / 1.3 - (Minecraft.getMinecraft().thePlayer.posY + Minecraft.getMinecraft().thePlayer.getEyeHeight());
        final double positions = MathHelper.sqrt_double(positionX * positionX + positionZ * positionZ);
        final float yaw = (float)(Math.atan2(positionZ, positionX) * 180.0 / 3.141592653589793) - 90.0f;
        final float pitch = (float)(-(Math.atan2(positionY, positions) * 180.0 / 3.141592653589793));
        return new float[] { yaw, pitch };
    }

    public static float[] getRotationFromPosition(final double x, final double z, final double y) {
        final double xDiff = x - Minecraft.getMinecraft().thePlayer.posX;
        final double zDiff = z - Minecraft.getMinecraft().thePlayer.posZ;
        final double yDiff = y - Minecraft.getMinecraft().thePlayer.posY;
        final double dist = MathHelper.sqrt_double(xDiff * xDiff + zDiff * zDiff);
        final float yaw = (float)(Math.atan2(zDiff, xDiff) * 180.0 / 3.141592653589793) - 90.0f;
        final float pitch = (float)(-(Math.atan2(yDiff, dist) * 180.0 / 3.141592653589793));
        return new float[] { yaw, pitch };
    }

    public static float getTrajAngleSolutionLow(final float d3, final float d1, final float velocity) {
        final float g = 0.006f;
        final float sqrt = velocity * velocity * velocity * velocity - g * (g * d3 * d3 + 2.0f * d1 * velocity * velocity);
        return (float)Math.toDegrees(Math.atan((velocity * velocity - Math.sqrt(sqrt)) / (g * d3)));
    }

    public static float getNewAngle(float angle) {
        angle %= 360.0f;
        if (angle >= 180.0f) {
            angle -= 360.0f;
        }
        if (angle < -180.0f) {
            angle += 360.0f;
        }
        return angle;
    }

    public static float getDistanceBetweenAngles(final float angle1, final float angle2) {
        float angle3 = Math.abs(angle1 - angle2) % 360.0f;
        if (angle3 > 180.0f) {
            angle3 = 360.0f - angle3;
        }
        return angle3;
    }

}
