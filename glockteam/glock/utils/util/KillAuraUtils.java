package glockteam.glock.utils.util;

import glockteam.glock.events.UpdateEvent;
import glockteam.glock.utils.AbstractUtil;
import glockteam.glock.utils.file.Value;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.util.MathHelper;

/**
 * Created by Kix on 12/22/2016.
 */
public class KillAuraUtils extends AbstractUtil {

    public void attackTarget(EntityLivingBase entity) {
        float sharpLevel = EnchantmentHelper.func_152377_a(mc.thePlayer.getHeldItem(), entity.getCreatureAttribute());
        mc.thePlayer.swingItem();
        mc.thePlayer.sendQueue.addToSendQueue(new C02PacketUseEntity(entity, C02PacketUseEntity.Action.ATTACK));
    }

    public Entity getBestEntity(double range, float fov, boolean rayTrace, int ticksExisted, boolean invisibles,
                                boolean players, boolean mobs, boolean animals) {
        Entity tempEntity = null;
        double dist = range;
        for (Object o : mc.theWorld.loadedEntityList) {
            boolean isValidEntity = ((mobs) && ((o instanceof EntityMob)) && (!((Entity) o).isInvisible())
                    && (!(o instanceof EntityAnimal)) && (!(o instanceof EntityPlayer)))
                    || ((animals) && ((o instanceof EntityAnimal)) && (!((Entity) o).isInvisible())
                    && (!(o instanceof EntityMob)) && (!(o instanceof EntityPlayer)))
                    || ((players) && ((o instanceof EntityPlayer)) && (!((Entity) o).isInvisible())
                    && (!(o instanceof EntityAnimal)) && (!(o instanceof EntityMob)));
            if (isValidEntity) {
                Entity entity = (Entity) o;
                if (shouldHitEntity(entity, range, fov, rayTrace, ticksExisted, invisibles, players, mobs, animals)) {
                    double curDist = mc.thePlayer.getDistanceToEntity(entity);
                    if (curDist <= dist) {
                        dist = curDist;
                        tempEntity = entity;
                    }
                }
            }
        }
        return tempEntity;
    }

    public boolean shouldHitEntity(Entity e, double range, float fov, boolean rayTrace, int ticksExisted,
                                   boolean invisibles, boolean players, boolean mobs, boolean animals) {
        boolean isAlive = (e instanceof EntityLivingBase);
        boolean isNotMe = (e != mc.thePlayer);
        boolean isNotNull = (e != null);
        boolean isInRange = (mc.thePlayer.getDistanceToEntity(e) <= range);
        boolean isInFov = (this.isWithinFOV(e, fov));
        boolean isNotDead = !(e.isDead);
        boolean canSeeEntity = ((mc.thePlayer.canEntityBeSeen(e)));
        boolean ticks = (e.ticksExisted >= ticksExisted);
        boolean isNotFakeDummie = e.getName() != mc.thePlayer.getName();
        boolean hasName = StringUtil.stripControlCodes(e.getName()).replaceAll(" ", "").length() > 0;
        boolean isMineplexBody = e instanceof EntityPlayer && e.getName().toLowerCase().startsWith("body #");
        if (rayTrace) {
            return (isAlive) && (isNotFakeDummie) && (isNotDead) && (canSeeEntity) && (ticks) && (isInFov) && (isNotMe)
                    && (isNotNull) && (isInRange) && !isMineplexBody && hasName;
        }
        return (isAlive) && (isNotFakeDummie) && (isNotDead) && (ticks) && (isInFov) && (isNotMe) && (isNotNull)
                && (isInRange) && !(isMineplexBody) && (hasName);
    }

    /**
     * Compares the needed yaw to the player's yaw and returns whether or not it
     * is less than the fov.
     *
     * @return True if the entity is within the FOV specified.
     */
    public boolean isWithinFOV(Entity entity, float fov) {
        float[] rotations = getRotationsNeeded(entity);
        float yawDifference = getYawDifference(mc.thePlayer.rotationYaw % 360F, rotations[0]);
        return yawDifference < fov && yawDifference > -fov;
    }

    /**
     * @return the difference between two yaw values
     */
    public float getYawDifference(float currentYaw, float neededYaw) {
        float yawDifference = neededYaw - currentYaw;
        if (yawDifference > 180)
            yawDifference = -((360F - neededYaw) + currentYaw);
        else if (yawDifference < -180)
            yawDifference = ((360F - currentYaw) + neededYaw);

        return yawDifference;
    }

    /**
     * @return Rotations needed to face the position.
     */
    public float[] getRotationsNeeded(double x, double y, double z) {
        double xSize = x - mc.thePlayer.posX;
        double ySize = y - (mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight());
        double zSize = z - mc.thePlayer.posZ;

        double theta = (double) MathHelper.sqrt_double(xSize * xSize + zSize * zSize);
        float yaw = (float) (Math.atan2(zSize, xSize) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) (-(Math.atan2(ySize, theta) * 180.0D / Math.PI));
        return new float[] {
                (mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw)) % 360F,
                (mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch))
                        % 360F, };
    }

    /**
     * @return Rotations needed to face the entity.
     */
    public float[] getRotationsNeeded(Entity entity) {
        if (entity == null)
            return null;
        return getRotationsNeeded(entity.posX, entity.posY + ((double) entity.getEyeHeight() / 2F), entity.posZ);
    }

    public void faceEntity(Entity entity, UpdateEvent event) {
        double diffX = entity.posX - this.mc.thePlayer.posX;
        double diffZ = entity.posZ - this.mc.thePlayer.posZ;
        double diffY = entity.posY + entity.getEyeHeight() - this.mc.thePlayer.posY
                + this.mc.thePlayer.getEyeHeight() * -2.0F + 1.2999999523162842D;

        double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / 3.141592653589793D) - 90.0F;
        float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / 3.141592653589793D);
        event.pitch += MathHelper.wrapAngleTo180_float(pitch - event.pitch);
        event.yaw += MathHelper.wrapAngleTo180_float(yaw - event.yaw);
    }

}
