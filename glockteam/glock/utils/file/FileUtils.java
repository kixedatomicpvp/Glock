package glockteam.glock.utils.file;

import glockteam.glock.Glock;
import glockteam.glock.module.Module;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Kix on 12/22/2016.
 */
public class FileUtils {
    public static File directory = new File(System.getProperty("user.home"), "Glock");
    public static File macros = new File(directory, "macros.settings");

    public static void overlookConfig() {
        if (!directory.exists()) {
            directory.mkdir();
        }
        if (!macros.exists()) {
            try {
                macros.createNewFile();
                try {
                    PrintStream binds = new PrintStream(macros);
                    for (Module module : Glock.moduleManager.getContents()) {
                        binds.println(module.getName() + " - " + module.getBind());
                    }
                    binds.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeBinds() {
        overlookConfig();
        try {
            PrintStream binds = new PrintStream(macros);
            for (Module m : Glock.moduleManager.getContents()) {
                binds.println(m.getName() + " - " + m.getBind());
            }
            binds.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void init() {
        FileUtils.overlookConfig();
        try {
            BufferedReader binds = new BufferedReader(new FileReader(macros));
            String bind = "";
            while ((bind = binds.readLine()) != null) {
                Module m = Glock.moduleManager.find(bind.toString().split(":")[0]);
                if (m != null) {
                    m.setBind(Integer.parseInt(bind.toString().split(":")[1]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
