package glockteam.glock.utils.file;

import glockteam.glock.Glock;

import java.io.File;
import java.io.IOException;

/**
 * Created by Kix on 12/22/2016.
 */
public abstract class AbstractFile {
    protected final String name, extension;
    protected final File file;

    public AbstractFile(String name, String extension) {
        this.name = name;
        this.extension = extension;
        file = new File(
                FileUtils.directory +File.separator + name + "." + extension);
        try {
            if ((!file.exists() || file.isDirectory()) && file.createNewFile()) {
                save();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Glock.fileManager.getContents().add(this);
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }

    public File getFile() {
        return file;
    }

    public abstract void load() throws IOException;

    public abstract void save() throws IOException;

}
