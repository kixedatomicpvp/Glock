package glockteam.glock.utils;

import glockteam.glock.utils.util.ColorUtil;
import glockteam.glock.utils.util.Reference;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Created by Kix on 12/23/2016.
 */
public class R2DUtils extends Reference{
        public void drawStringWithShadow(final String text, final float x, final float y, final int color) {
            mc.fontRendererObj.drawStringWithShadow(text, x, y, color);
        }

        public void drawSmallString(final String s, final int x, final int y, final int color) {
            GL11.glPushMatrix();
            GL11.glScalef(0.5f, 0.5f, 0.5f);
            this.drawStringWithShadow(s, x * 2, y * 2, color);
            GL11.glPopMatrix();
        }

        public static void enableGL2D() {
            GL11.glDisable(2929);
            GL11.glEnable(3042);
            GL11.glDisable(3553);
            GL11.glBlendFunc(770, 771);
            GL11.glDepthMask(true);
            GL11.glEnable(2848);
            GL11.glHint(3154, 4354);
            GL11.glHint(3155, 4354);
        }

        public static void disableGL2D() {
            GL11.glEnable(3553);
            GL11.glDisable(3042);
            GL11.glEnable(2929);
            GL11.glDisable(2848);
            GL11.glHint(3154, 4352);
            GL11.glHint(3155, 4352);
        }

        public static void drawRect(final float x, final float y, final float x1, final float y1, final int color) {
            enableGL2D();
            ColorUtil.glColor(color);
            drawRect(x, y, x1, y1);
            disableGL2D();
        }

        public static void drawBorderedRect(final float x, final float y, final float x1, final float y1, final float width, final int internalColor, final int borderColor) {
            enableGL2D();
            ColorUtil.glColor(internalColor);
            drawRect(x + width, y + width, x1 - width, y1 - width);
            ColorUtil.glColor(borderColor);
            drawRect(x + width, y, x1 - width, y + width);
            drawRect(x, y, x + width, y1);
            drawRect(x1 - width, y, x1, y1);
            drawRect(x + width, y1 - width, x1 - width, y1);
            disableGL2D();
        }

        public static void drawBorderedRect(float x, float y, float x1, float y1, final int insideC, final int borderC) {
            enableGL2D();
            x *= 2.0f;
            x1 *= 2.0f;
            y *= 2.0f;
            y1 *= 2.0f;
            GL11.glScalef(0.5f, 0.5f, 0.5f);
            drawVLine(x, y, y1, borderC);
            drawVLine(x1 - 1.0f, y, y1, borderC);
            drawHLine(x, x1 - 1.0f, y, borderC);
            drawHLine(x, x1 - 2.0f, y1 - 1.0f, borderC);
            drawRect(x + 1.0f, y + 1.0f, x1 - 1.0f, y1 - 1.0f, insideC);
            GL11.glScalef(2.0f, 2.0f, 2.0f);
            disableGL2D();
        }

        public static void drawGradientRect(final float x, final float y, final float x1, final float y1, final int topColor, final int bottomColor) {
            enableGL2D();
            GL11.glShadeModel(7425);
            GL11.glBegin(7);
            ColorUtil.glColor(topColor);
            GL11.glVertex2f(x, y1);
            GL11.glVertex2f(x1, y1);
            ColorUtil.glColor(bottomColor);
            GL11.glVertex2f(x1, y);
            GL11.glVertex2f(x, y);
            GL11.glEnd();
            GL11.glShadeModel(7424);
            disableGL2D();
        }

        public static void drawHLine(float x, float y, final float x1, final int y1) {
            if (y < x) {
                final float var5 = x;
                x = y;
                y = var5;
            }
            drawRect(x, x1, y + 1.0f, x1 + 1.0f, y1);
        }

        public static void drawVLine(final float x, float y, float x1, final int y1) {
            if (x1 < y) {
                final float var5 = y;
                y = x1;
                x1 = var5;
            }
            drawRect(x, y + 1.0f, x + 1.0f, x1, y1);
        }

        public static void drawHLine(float x, float y, final float x1, final int y1, final int y2) {
            if (y < x) {
                final float var5 = x;
                x = y;
                y = var5;
            }
            drawGradientRect(x, x1, y + 1.0f, x1 + 1.0f, y1, y2);
        }

        public static void drawRect(final float x, final float y, final float x1, final float y1) {
            GL11.glBegin(7);
            GL11.glVertex2f(x, y1);
            GL11.glVertex2f(x1, y1);
            GL11.glVertex2f(x1, y);
            GL11.glVertex2f(x, y);
            GL11.glEnd();
        }

        public static void drawTri(final double x1, final double y1, final double x2, final double y2, final double x3, final double y3, final double width, final Color c) {
            GL11.glEnable(3042);
            GL11.glDisable(3553);
            GL11.glEnable(2848);
            GL11.glBlendFunc(770, 771);
            ColorUtil.glColor(c.getRGB());
            GL11.glLineWidth((float)width);
            GL11.glBegin(3);
            GL11.glVertex2d(x1, y1);
            GL11.glVertex2d(x2, y2);
            GL11.glVertex2d(x3, y3);
            GL11.glEnd();
            GL11.glDisable(2848);
            GL11.glEnable(3553);
            GL11.glDisable(3042);
        }

        public static void drawFilledCircle(final int x, final int y, final double radius, final Color c) {
            ColorUtil.glColor(c.getRGB());
            GlStateManager.enableAlpha();
            GlStateManager.enableBlend();
            GL11.glDisable(3553);
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            GlStateManager.alphaFunc(516, 0.001f);
            final Tessellator tess = Tessellator.getInstance();
            final WorldRenderer render = tess.getWorldRenderer();
            for (double i = 0.0; i < 360.0; ++i) {
                final double cs = i * 3.141592653589793 / 180.0;
                final double ps = (i - 1.0) * 3.141592653589793 / 180.0;
                final double[] outer = { Math.cos(cs) * radius, -Math.sin(cs) * radius, Math.cos(ps) * radius, -Math.sin(ps) * radius };
                render.startDrawing(6);
                render.addVertex(x + outer[2], y + outer[3], 0.0);
                render.addVertex(x + outer[0], y + outer[1], 0.0);
                render.addVertex(x, y, 0.0);
                tess.draw();
            }
            GlStateManager.disableBlend();
            GlStateManager.alphaFunc(516, 0.1f);
            GlStateManager.disableAlpha();
            GL11.glEnable(3553);
        }
    }
